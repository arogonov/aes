const CryptoJS = require("crypto-js");

const decrypt = (cipherText, secret) => {
  const cipherTextWidthPadding = cipherText.replace(/\-/g, '+').replace(/_/g, '/');
  const bytes  = CryptoJS.AES.decrypt(cipherTextWidthPadding, secret);
  return bytes.toString(CryptoJS.enc.Utf8);
}

module.exports = decrypt;
