const CryptoJS = require("crypto-js");

const encrypt = (message, secret) => {
  const cipherText = CryptoJS.AES.encrypt(message , secret).toString();
  const cipherTextWithoutPadding = cipherText.replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');
  return cipherTextWithoutPadding;
}

module.exports = encrypt;
