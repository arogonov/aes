const decrypt = require("./src/decrypt");
const encrypt = require("./src/encrypt");

const testMessage = 'message';
const testSecret = 'secret';

const base64WithoutPadding = encrypt(testMessage, testSecret)
const originalMessage = decrypt(base64WithoutPadding, testSecret)

console.log('-----Тестовые данные-------');
console.log(testMessage);
console.log(testSecret);
console.log(base64WithoutPadding);
console.log(originalMessage);

// ad = 103
const tk = 'U2FsdGVkX1-zUprmMak6LdmGc0iYPIPxBUKNnS4reQ4'
const secret = '967018';
const ad = decrypt(tk, secret);

console.log('-----Реальные данные-------');
console.log(tk);
console.log(secret);
console.log(ad);